data "aws_vpc" "this" {
  filter {
    name   = "tag:Name"
    values = ["kops-vpc"]
  }
}

data "aws_subnet" "private-a" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this.id]
  }
  filter {
    name   = "tag:SubnetType"
    values = ["Private"]
  }
  filter {
    name = "availability-zone"
    values = ["${data.aws_region.current.name}a"]
  }
}

data "aws_subnet" "private-b" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this.id]
  }
  filter {
    name   = "tag:SubnetType"
    values = ["Private"]
  }
  filter {
    name = "availability-zone"
    values = ["${data.aws_region.current.name}b"]
  }
}

data "aws_subnet" "public-a" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this.id]
  }
  filter {
    name   = "tag:SubnetType"
    values = ["Public"]
  }
  filter {
    name = "availability-zone"
    values = ["${data.aws_region.current.name}a"]
  }
}

data "aws_subnet" "public-b" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.this.id]
  }
  filter {
    name   = "tag:SubnetType"
    values = ["Public"]
  }
  filter {
    name = "availability-zone"
    values = ["${data.aws_region.current.name}b"]
  }
}



resource "aws_security_group" "base" {
  name        = "${local.app_name}-sg"
  description = "Default security group for ${local.app_name}"
  vpc_id      = data.aws_vpc.this.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [data.aws_vpc.this.cidr_block, "100.36.158.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
