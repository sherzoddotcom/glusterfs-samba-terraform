data "aws_region" "current" {}

locals {
  app_name = var.tags.application
}

data "aws_ami" "ubuntu" {
  owners = ["099720109477"]
  name_regex = "^ubuntu/images/hvm-ssd/ubuntu-jammy-22\\.04-amd64-server.*"
  most_recent = true
}

data "aws_ami" "al2" {
  owners = ["amazon"]
  name_regex = "^amzn2-ami-kernel-5.10-hvm-2.0.20221210.1-x86_64-gp2"
  most_recent = true
}

data "aws_ami" "win2022" {
  owners = ["amazon"]
  name_regex = "^Windows_Server-2022-English-Full-Base-.*"
  most_recent = true
}


module "iam" {
  source = "./modules/ec2-iam"
  name = "GlusterSambaEC2Role"
}


module "nodes" {
  for_each = {
    node1 = {
      subnet = data.aws_subnet.public-a.id
      secondary_ips = 1
    } 
    node2 = {
      subnet = data.aws_subnet.public-b.id
      secondary_ips = 0
    } 
    # node3 = {subnet = data.aws_subnet.private-b.id} 
  }

  source = "./modules/ec2"
  ami_id = data.aws_ami.ubuntu.id
  name = "${local.app_name}-${each.key}"
  iam_role = module.iam.instance_profile_name
  subnet = each.value.subnet  
  security_groups = [aws_security_group.base.id]
  ssh_keyname = var.ssh_key_name
  secondary_ips = each.value.secondary_ips
  disks = [
    {
      size = 1
      device_name = "/dev/xvdb"
    }
  ]
  tags = {
    Type = "node"
    "ansible-var" = jsonencode({
      name = each.key
      master = each.key  == "node1" ? 1 : 0
    })
    "ansible-group" = each.key  == "node1" ? "master" : ""
  }
}


module "bastion" {
  source = "./modules/ec2"
  ami_id = data.aws_ami.ubuntu.id
  name = "${local.app_name}-bastion"
  iam_role = module.iam.instance_profile_name
  subnet = data.aws_subnet.public-a.id
  security_groups = [aws_security_group.base.id]
  ssh_keyname = var.ssh_key_name
  secondary_ips = 0
}


module "bastion-win" {
  source = "./modules/ec2"
  ami_id = data.aws_ami.win2022.id
  name = "${local.app_name}-bastion-win"
  iam_role = module.iam.instance_profile_name
  subnet = data.aws_subnet.public-a.id
  security_groups = [aws_security_group.base.id]
  ssh_keyname = var.ssh_key_name
  secondary_ips = 0
  root_disk_size = 30
}
