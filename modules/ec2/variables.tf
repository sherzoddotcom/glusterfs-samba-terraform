variable "name" {}
variable "type" {
  default = "t3a.small"
}

variable "root_disk_size" {
  type = number
  default = 8
}
variable "iam_role" {}
variable "ami_id" {}
variable "subnet" {}
variable "security_groups" {
  type = list(string)
}
variable "ssh_keyname" {}
variable "disks" {
  default = []
  type = list(object({
    size = number
    device_name = string
  }))

  validation {
    condition = alltrue([
      for d in var.disks: d.size > 0
    ])
    error_message = "Disk size must be 1GB minimum."
  }
}

variable "tags" {
  type = map(any)
  default  = {}
}
 
variable "secondary_ips" {
  type = number
  default = 0
}
