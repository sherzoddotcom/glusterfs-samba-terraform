output "instance_id" {
    value = aws_instance.this.id
}

output "secondary_ips" {
    # value = {"${var.name}" = one(setsubtract(aws_network_interface.this.private_ip_list, [aws_network_interface.this.private_ip]))}
    value = {"${var.name}" = coalesce(one(aws_instance.this.secondary_private_ips), "-")}
}

// TODO Need to make this static
output "vip" {
    # value = {"${var.name}" = one(setsubtract(aws_network_interface.this.private_ip_list, [aws_network_interface.this.private_ip]))}
    value = coalesce(one(aws_instance.this.secondary_private_ips), "-")
}


output "public_ip" {
    value = aws_instance.this.public_ip
}