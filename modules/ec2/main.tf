data "aws_subnet" "this" {
  id = var.subnet
}

resource "aws_instance" "this" {
  ami = var.ami_id
  instance_type = var.type
  iam_instance_profile = var.iam_role
  # subnet_id = var.subnet
  # vpc_security_group_ids = var.security_groups
  key_name = var.ssh_keyname

  # secondary_private_ips = var.secondary_ips

  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.this.id
  }
  
  root_block_device {
    volume_size = var.root_disk_size
    volume_type = "gp3"
  }

  dynamic "ebs_block_device" {
    for_each = var.disks
    iterator = disk
    content {
      device_name = disk.value["device_name"]
      volume_type = "gp3"
      volume_size = disk.value["size"]
    }
  }

  tags = merge(  {
    Name = var.name
  },
  var.tags
  )

}

resource "aws_network_interface" "this" {
  subnet_id       = var.subnet
  private_ips_count = var.secondary_ips
  security_groups = var.security_groups
}

# resource "aws_ebs_volume" "disks" {
#   for_each = var.disks
#   availability_zone = data.aws_subnet.this.availability_zone
#   size = each.size
#   tags = {
#     Name = ""
#   }
# } 