resource "aws_iam_instance_profile" "this" {
  name = var.name
  role = aws_iam_role.role.name
}

data "aws_iam_policy" "ssmcore" {
  name = "AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role" "role" {
  name = var.name
  managed_policy_arns = concat([data.aws_iam_policy.ssmcore.arn], var.policies)
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}