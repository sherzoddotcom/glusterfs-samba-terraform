data "aws_route53_zone" "primary" {
  name         = var.domain_name
}

resource "aws_route53_record" "bastion" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "bastion.${data.aws_route53_zone.primary.name}"
  type    = "A"
  ttl     = 300
  records = [module.bastion.public_ip]
}

// TODO Need a way to read active node from a registry
resource "aws_route53_record" "smb-vip" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "smb.${data.aws_route53_zone.primary.name}"
  type    = "A"
  ttl     = 300
  records = [module.nodes["node1"].vip]
}

