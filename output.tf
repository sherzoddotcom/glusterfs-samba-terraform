output "nodes" {
    value = [for n in module.nodes: n.secondary_ips]
}

output "bastion" {
    value = module.bastion.public_ip
}