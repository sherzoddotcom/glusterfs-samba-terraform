variable "tags" {
  description = "Global tags"
  type = object({
    environment = string
    application = string
  })
}

variable "vip" {
  description = "Virtual IP of the master. In AWS, this needs to be a secondary IP and assigned to master node"
}

variable "ssh_key_name" {
  description = "SSH Key pair name in AWS, used for accessing the nodes with Ansible."
}

variable "domain_name" {
  description = "Route53 public zone name"
}